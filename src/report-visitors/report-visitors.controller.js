/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name report-visitors.controller:VisitorsController
     *
     * @description/report-visitors.controller.spec.js:
     * Controller for visitors report.
     */
    angular
        .module('report-visitors')
        .controller('VisitorsController', controller);

    controller.$inject = ['openlmisUrlFactory', 'reportVisitorsService'];

    function controller(openlmisUrlFactory, reportVisitorsService) {
        var vm = this;

        vm.getExportLink = getExportLink;

        /**
         * @ngdoc property
         * @propertyOf report-visitors.controller:VisitorsController
         * @name startDate
         * @type {Date}
         *
         * @description
         * The start date filter.
         */
        vm.startDate = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-visitors.controller:VisitorsController
         * @name endDate
         * @type {Date}
         *
         * @description
         * The end date filter
         */
        vm.endDate = undefined;

        /**
         * @ngdoc method
         * @methodOf report-visitors.controller:VisitorsController
         * @name getExportLink
         *
         * @description
         * Returns URL for downloading visitors report
         *
         * @return {String} the URL for downloading visitor report
         */

        function getExportLink() {
            return reportVisitorsService.getDownloadLink(vm.startDate, vm.endDate);
        }
    }
})();
