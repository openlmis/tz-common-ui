/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc controller
     * @name report-quantification.controller:QuantificationExportController
     *
     * @description
     * Controller for quantification report page.
     */
    angular
        .module('report-quantification')
        .controller('QuantificationExportController', controller);

    controller.$inject = ['$scope', 'openlmisUrlFactory', 'geographicZones', 'programs', 'YEAR_SERVICES',
        'processingPeriods', '$timeout', '$filter', '$q'];

    function controller($scope, openlmisUrlFactory, geographicZones, programs, YEAR_SERVICES,
                        processingPeriods, $timeout, $filter, $q) {
        var vm = this;

        vm.$onInit = onInit;
        vm.getDownloadUrl = getDownloadUrl;
        vm.changeYearFilter = changeYearFilter;

        /**
         * @ngdoc property
         * @propertyOf report-quantification.controller:QuantificationExportController
         * @name years
         * @type {Array}
         *
         * @description
         * The list of all years available.
         */
        vm.years = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-quantification.controller:QuantificationExportController
         * @name periods
         * @type {Array}
         *
         * @description
         * The list of all filtered periods for a given year.
         */
        vm.periods = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-quantification.controller:QuantificationExportController
         * @name showPeriod
         * @type {Boolean}
         *
         * @description
         * It shows the list of periods available for particular year.
         */
        vm.showPeriod = false;

        /**
         * @ngdoc property
         * @propertyOf report-quantification.controller:QuantificationExportController
         * @name geographicZones
         * @type {Array}
         *
         * @description
         * The list of all geographic Zones available to the user.
         */
        vm.geographicZones = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-quantification.controller:QuantificationExportController
         * @name programs
         * @type {Array}
         *
         * @description
         * The list of all programs available.
         */
        vm.programs = undefined;

        /**
         * @ngdoc property
         * @propertyOf  report-quantification.controller:QuantificationExportController
         * @name processingPeriods
         * @type {List}
         *
         * @description
         * The list of all periods displayed in the table.
         */
        vm.processingPeriods = undefined;

        /**
         * @ngdoc method
         * @methodOf report-quantification.controller:QuantificationExportController
         * @name $onInit
         *
         * @description
         * Initialization method called after the controller has been created. Responsible for
         * setting data to be available on the view.
         */
        function onInit() {
            vm.geographicZones = geographicZones;
            vm.programs = programs;
            vm.years = YEAR_SERVICES;
            vm.processingPeriods = processingPeriods;
        }

        /**
         * @ngdoc method
         * @methodOf report-quantification.controller:QuantificationExportController
         * @name getDownloadUrl
         *
         * @description
         * Prepares a download URL for the given quantification report parameters.
         *
         * @param  {Object} quantification the quantification to prepare the URL for
         * @return {String}       the prepared URL
         */
        function getDownloadUrl(vm) {
            var url = '/api/districtLevelQuantifications?district=' + vm.geographicZone.id;

            return openlmisUrlFactory(url);
        }

        $scope.$watch('vm.year1', function() {
            vm.showPeriod = false;
            if (vm.year !== undefined) {
                vm.showPeriod = true;

                vm.periods = filteredPeriods(vm.year);

            }
        });

        function changeYearFilter() {
            return filteredPeriods();
        }

        function filteredPeriods() {

            var promise = $q(function(resolve) {
                vm.showPeriod = false;

                $timeout(function() {

                    var periods = $filter('filter')(vm.processingPeriods.content, function(period) {
                        var filteredYear = new Date(period.startDate).getFullYear();

                        return parseInt(filteredYear, 10) === parseInt(vm.year.name, 10);
                    });

                    resolve(periods);

                }, 2000);
            });

            promise.then(function(data) {
                vm.periods = data;
                vm.showPeriod = true;
            });

        }

    }
})();
