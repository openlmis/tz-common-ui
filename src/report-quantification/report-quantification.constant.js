/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    /**
     * @ngdoc object
     * @name report-quantification.YEAR_SERVICES
     *
     * @description
     * This is constant defining available list of years available.
     */
    angular
        .module('report-quantification')
        .constant('YEAR_SERVICES', years());

    function years() {

        var max = new Date().getFullYear();
        var min = max - 5;
        var years = [];

        for (var i = max; i >= min; i--) {
            var object = {};
            object.name = i;
            years.push(object);
        }
        return years;
    }

})();
