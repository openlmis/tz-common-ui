/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {
    'use strict';

    /**
     * @ngdoc controller
     * @name report-source-of-fund-summary.controller:SourceOfFundSummaryController
     *
     * @description
     * Controller for source of fund summary report page.
     */
    angular
        .module('report-source-of-fund-summary')
        .controller('SourceOfFundSummaryController', controller);

    controller.$inject = ['$scope', '$window', 'openlmisUrlFactory', 'accessTokenFactory',
        'geographicZones', 'programs', 'processingSchedules', 'YEAR_SERVICES',
        'sourceOfFundSummaryService', 'loadingModalService', '$timeout', '$filter'];

    function controller($scope, $window, openlmisUrlFactory, accessTokenFactory,
                        geographicZones, programs, processingSchedules, YEAR_SERVICES,
                        sourceOfFundSummaryService, loadingModalService) {
        var vm = this;

        vm.$onInit = onInit;
        vm.downloadReportUrl = downloadReportUrl;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name years
         * @type {Array}
         *
         * @description
         * The list of all years available.
         */
        vm.years = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name periods
         * @type {Array}
         *
         * @description
         * The list of all filtered periods for a given year.
         */
        vm.periods = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name schedules
         * @type {Array}
         *
         * @description
         * The list of processing schedules.
         */
        vm.schedules = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name facilities
         * @type {Array}
         *
         * @description
         * The list of all facilities.
         */
        vm.facilities = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name geographicZones
         * @type {Array}
         *
         * @description
         * The list of all geographic Zones available to the user.
         */
        vm.geographicZones = undefined;

        /**
         * @ngdoc property
         * @propertyOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name programs
         * @type {Array}
         *
         * @description
         * The list of all programs available.
         */
        vm.programs = undefined;

        /**
         * @ngdoc property
         * @propertyOf  report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name processingPeriods
         * @type {List}
         *
         * @description
         * The list of all periods displayed in the table.
         */
        vm.processingPeriods = undefined;

        /**
         * @ngdoc method
         * @methodOf report-source-of-fund-summary.controller:SourceOfFundSummaryController
         * @name $onInit
         *
         * @description
         * Initialization method called after the controller has been created. Responsible for
         * setting data to be available on the view.
         */
        function onInit() {
            vm.geographicZones = geographicZones;
            vm.programs = programs;
            vm.years = YEAR_SERVICES;
            vm.schedules = processingSchedules;
            vm.processingPeriods = [];
            vm.periods = [];
        }

        function filterPeriods() {
            vm.periods = [];
            if (vm.processingPeriods) {
                vm.periods = vm.processingPeriods.filter(
                    function(period) {

                        var matchYear = true;
                        if (vm.year) {
                            var filteredYear = new Date(period.startDate).getFullYear();
                            matchYear = parseInt(filteredYear, 10) === parseInt(vm.year.name, 10);
                        }

                        var matchSchedule = true;
                        if (vm.schedule) {
                            matchSchedule = period.processingSchedule.id === vm.schedule.id;
                        }

                        return matchYear && matchSchedule;

                    }
                );
            }
        }

        function downloadReportUrl() {
            $window.open(
                accessTokenFactory.addAccessToken(
                    sourceOfFundSummaryService.buildSourceOfFundSummaryUrl(
                        vm.zone,
                        vm.facility,
                        vm.program,
                        vm.period,
                        vm.format
                    )
                ),
                '_blank'
            );
        }

        function loadProcessingPeriods() {
            return sourceOfFundSummaryService.getProcessingPeriods(vm.program, vm.facility)
                .then(function(periods) {
                    vm.processingPeriods = periods.content;
                    vm.periods = periods.content;

                    filterPeriods();

                });
        }

        function loadFacilities() {
            return sourceOfFundSummaryService.getZoneFacilities(vm.zone)
                .then(function(facilities) {
                    vm.facilities = facilities.content;
                });
        }

        $scope.$watch('vm.year', function() {
            filterPeriods();
        });

        $scope.$watch('vm.schedule', function() {
            filterPeriods();
        });

        $scope.$watch('vm.zone', function() {
            vm.facilities = [];
            vm.periods = [];
            if (vm.zone) {
                loadingModalService.open();
                loadFacilities().then(function() {
                    loadingModalService.close();
                });
            }
        });

        $scope.$watch('vm.facility', function() {
            vm.periods = [];

            loadingModalService.open();
            loadProcessingPeriods().then(function() {
                loadingModalService.close();
            });

        });

        $scope.$watch('vm.program', function() {

            loadingModalService.open();
            loadProcessingPeriods().then(function() {
                loadingModalService.close();
            });

        });
    }
})();
