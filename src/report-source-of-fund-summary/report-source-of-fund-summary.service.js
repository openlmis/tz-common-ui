/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */
(function() {

    'use strict';

    /**
     *
     * @ngdoc service
     * @name report-source-of-fund-summary.sourceOfFundSummaryService.
     *
     * @description
     * Responsible for retrieving requisition reports from the server.
     */
    angular
        .module('report-source-of-fund-summary')
        .service('sourceOfFundSummaryService', service);

    service.$inject = ['openlmisUrlFactory', 'periodService', 'facilityService',
        'processingScheduleService', '$resource', '$http'];

    function service(openlmisUrlFactory, periodService, facilityService, processingScheduleService) {
        this.getProcessingPeriods = getProcessingPeriods;
        this.getZoneFacilities = getZoneFacilities;
        this.getProcessingSchedules = getProcessingSchedules;
        this.buildSourceOfFundSummaryUrl = buildSourceOfFundSummaryUrl;

        function getProcessingPeriods(program, facility) {
            var params = {};
            if (program) {
                params.programId = program.id;
            }
            if (facility) {
                params.facilityId = facility.id;
            }
            return periodService.query(params);
        }

        function getProcessingSchedules(program, facility) {
            var params = {
                programId: program.id
            };
            if (facility) {
                params.facilityId = facility.id;
            }
            return processingScheduleService.query(params);
        }

        function getZoneFacilities(zone) {
            return facilityService.query(
                {
                    zoneId: zone.id,
                    recurse: true
                }
            );
        }

        function buildSourceOfFundSummaryUrl(zone, facility, program, period, format) {
            if ((zone || facility) && program && period && format) {
                var url = '/api/sourceOfFunds/zoneSummaries/' + format + '?zoneId=' + zone.id;
                if (facility) {
                    url = '/api/sourceOfFunds/facilitySummaries/' + format + '?facilityId=' + facility.id;
                }
                url = url + '&programId=' + program.id + '&periodId=' + period.id;
                return openlmisUrlFactory(url);
            }
            return '';

        }
    }
})();
