/*
 * This program is part of the OpenLMIS logistics management information system platform software.
 * Copyright © 2017 VillageReach
 *
 * This program is free software: you can redistribute it and/or modify it under the terms
 * of the GNU Affero General Public License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *  
 * This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY;
 * without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. 
 * See the GNU Affero General Public License for more details. You should have received a copy of
 * the GNU Affero General Public License along with this program. If not, see
 * http://www.gnu.org/licenses.  For additional information contact info@OpenLMIS.org. 
 */

(function() {

    'use strict';

    angular
        .module('report-source-of-fund-summary')
        .config(config);

    config.$inject = ['$stateProvider', 'REPORT_RIGHTS'];

    function config($stateProvider, REPORT_RIGHTS) {

        $stateProvider.state('openlmis.reports.source-of-fund-summary', {
            controller: 'SourceOfFundSummaryController',
            controllerAs: 'vm',
            label: 'report.sourceOfFundSummary',
            showInNavigation: true,
            templateUrl: 'report-source-of-fund-summary/report-source-of-fund-summary.html',
            url: '/fund-source-summary',
            accessRights: [
                REPORT_RIGHTS.REPORTS_VIEW
            ],

            resolve: {
                programs: function(programService) {
                    return programService.getAll();
                },

                geographicZones: function($q, geographicZoneService) {
                    var deferred = $q.defer();

                    geographicZoneService.getAll().then(
                        function(response) {
                            deferred.resolve(response.content);
                        },
                        deferred.reject
                    );

                    return deferred.promise;
                },
                processingSchedules: function($q, processingScheduleService) {
                    var deferred = $q.defer();

                    processingScheduleService.query({}).then(
                        function(response) {
                            deferred.resolve(response.content);
                        },
                        deferred.reject
                    );

                    return deferred.promise;
                }
            }
        });

    }

})();